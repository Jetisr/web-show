var variables = {}; // Experimenting with namespaces for this project
var functions = {};

// Vars
variables.streamers = {
};
variables.streamersArray = ["ESL_SC2", "OgamingSC2", "cretetion", "freecodecamp", "storbeck", "habathcx", "RobotCaleb", "noobs2ninjas", "brunofin", "comster404"];
variables.apiParameters = {
    "url": "https://api.twitch.tv/kraken/",
    "streams": "streams/",
};
variables.templates = {};
variables.templates.onlineTemplate = $.templates("<a href='{{:url}}' class='collection-item online' target='_blank'><i class='material-icons online'>done</i> {{:account}} is playing {{:game}}</a>");
variables.templates.offlineTemplate = $.templates("<a href='{{:url}}' class='collection-item offline' target='_blank'><i class='material-icons offline'>error</i> {{:account}} is offline</a>");
variables.templates.nonExistenceTemplate = $.templates("<li class='collection-item non-existence' target='_blank'><i class='material-icons non-existence'>warning</i> {{:account}} doesn't exist </li>");
variables.buttonStates = {};
variables.buttonStates.online = false;
variables.buttonStates.offline = false;
variables.buttonStates.nonexistence = false;

// Funcs
functions.getStreams = function(account) {
    $.getJSON(variables.apiParameters.url + variables.apiParameters.streams + account)
        .done(function(data) {
            functions.jsonParser(data, account);
        })
        .fail(function(data) {
            functions.jsonParser(data, account);
        });
};

functions.jsonParser = function(json, account) {
    variables.streamers[account] = {};
    if (json.error) {
        variables.streamers[account].status = "doesn't exist";
    } else if (json.stream === null) {
        variables.streamers[account].status = "offline";
        variables.streamers[account].url = "https://www.twitch.tv/" + account;
    } else {
        variables.streamers[account].status = "online";
        variables.streamers[account].game = json.stream.game;
        variables.streamers[account].viewers = json.stream.viewers;
        variables.streamers[account].url = json.stream.channel.url;
    }
    functions.prepHtml(account);
};

functions.prepHtml = function(account) {
    $("#preload").addClass("hide");
    if (variables.streamers[account].status == "online") {
        $("#data").append(variables.templates.onlineTemplate({url: variables.streamers[account].url, account: account, game: variables.streamers[account].game}));
    } else if (variables.streamers[account].status == "offline") {
        $("#data").append(variables.templates.offlineTemplate({url: variables.streamers[account].url, account: account}));
    } else {
        $("#data").append(variables.templates.nonExistenceTemplate({account: account}));
    }
};

functions.filter = function(buttonClicked) {
    if (buttonClicked == "online") {
        if (variables.buttonStates.online === false) {
            $(".online").removeClass("hide");
            $(".offline").addClass("hide");
            $(".non-existence").addClass("hide");
            variables.buttonStates.online = true;
            variables.buttonStates.offline = false;
            variables.buttonStates.nonexistence = false;
        } else {
            $(".offline").removeClass("hide");
            $(".non-existence").removeClass("hide");
            variables.buttonStates.online = false;
        }
    } else if (buttonClicked == "offline") {
        if (variables.buttonStates.offline === false) {
            $(".offline").removeClass("hide");
            $(".online").addClass("hide");
            $(".non-existence").addClass("hide");
            variables.buttonStates.offline = true;
            variables.buttonStates.online = false;
            variables.buttonStates.nonexistence = false;
        } else {
            $(".online").removeClass("hide");
            $(".non-existence").removeClass("hide");
            variables.buttonStates.offline = false;
        }
    } else {
        if (variables.buttonStates.nonexistence === false) {
            $(".non-existence").removeClass("hide");
            $(".offline").addClass("hide");
            $(".online").addClass("hide");
            variables.buttonStates.nonexistence = true;
            variables.buttonStates.online = false;
            variables.buttonStates.offline = false;
        } else {
            $(".offline").removeClass("hide");
            $(".online").removeClass("hide");
            variables.buttonStates.nonexistence = false;
        }
    }
};

$(document).ready(function() {
    for (var i = 0; i < variables.streamersArray.length; i++) {
        functions.getStreams(variables.streamersArray[i]);
    }
    $("#online-filter").click(function() {
        functions.filter("online");
    });
    $("#offline-filter").click(function() {
        functions.filter("offline");
    });
    $("#nonexistence-filter").click(function() {
        functions.filter("non-existence");
    });
    $(".searchButton").click(function() {
        functions.getStreams($("#searchField").val());
    });
    $("#searchField").keyup(function(e) {
        if (e.keyCode == 13) {
            functions.getStreams($("#searchField").val());
        }
    });
});