// Non-chained functions. They can be called from anywhere and will return a result, rather than calling another function.
function convertTemp(temp, convertFrom) { // Converts a numeric temperature to either celsius, or fahrenheit depending on what the second argument is.
	if (convertFrom == "C") {
		return (Math.round((temp * 9 / 5) + 32));
	} else {
		return (Math.round((temp - 32) * 5 / 9));
	}
}

function changeTempSymbol(sym) { // Changes the temperature symbol into either an F or a C, depending on the input
	if (sym == "C") {
		return "F";
	} else {
		return "C";
	}
}

// Chained functions. Each will call the next one when they're finished.
function getPosition(funcToCall) { // Requires the user to allow their location to be known
	navigator.geolocation.getCurrentPosition(funcToCall);
}

function getWeather(locData) { // Sends an AJAX request to forecast.io to get the current weather
	var apikey = "e6996dd84d9125f135c7fd6bfb748066";
	var url = "https://api.forecast.io/forecast/";
	var latitude = locData.coords.latitude;
	var longitude = locData.coords.longitude;
	$.getJSON(url + apikey + "/" + latitude + "," + longitude + "?units=si&callback=?", initialHTML);
}

function initialHTML(weather) { // Changes the initial HTML of the page
	var temperature = weather.currently.temperature;
	var conditions = weather.currently.summary.toLowerCase();
	$("#data").html(
		"<p class='flow-text center'>It is currently <span id='temp'>" + Math.round(temperature) + "</span> °<span id='tempSymbol'>C</span> and <span id='conditions'>" + conditions + "</span></p>");
}

// Calling the functions
$(document).ready(function() {
	getPosition(getWeather);
	$("#convert").click(function() {
		var temp = $("#temp").html();
		var tempSymbol = $("#tempSymbol").html();
		var cond = $("#conditions").html();
		$("#data").html(
			"<p class='flow-text center'>It is currently <span id='temp'>" + convertTemp(temp, tempSymbol) + "</span> °<span id='tempSymbol'>" + changeTempSymbol(tempSymbol) + "</span> and <span id='conditions'>" + cond + "</span></p>");
		$("#convert").html(function() {
			if (tempSymbol == "F") {
				return "Convert to Fahrenheit";
			} else {
				return "Convert to Celsius";
			}
		});
	});
});