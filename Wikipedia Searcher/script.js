function searchWikipedia(searchValue) {
    $("#cards>a").addClass("hide");
    if (searchValue === "") {
        alert("You need to enter something");
    } else {
        $.ajax({
                url: 'https://en.wikipedia.org/w/api.php?callback=?',
                type: "GET",
                dataType: "JSON",
                data: {
                    action: "opensearch",
                    format: "json",
                    search: searchValue
                }
            })
            .done(function(data) {
                console.log(data);
                console.log(data[1].length);
                console.log("success");
                for (var i = 1; i < data[1].length + 1; i++) {
                    for (var j = 0; j < i; j++) {
                        $("#title-" + [i]).html(data[1][j]);
                        $("#summary-" + [i]).html(data[2][j]);
                        $("#card-" + [i]).attr("href", data[3][j]);
                        $("#card-" + [i]).removeClass("hide");
                    }
                }

            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    }
}


$(document).ready(function() {
    $("#submit").click(function() {
        searchWikipedia($("#search").val());
    });
    $("#search").keyup(function(e) {
        if (e.keyCode == 13) {
            searchWikipedia($("#search").val());
        }
    });
});
